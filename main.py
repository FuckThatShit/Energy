import kivy
kivy.require('1.10.0')


import os, sys, shutil, random

from kivy.app import App
from kivy.base import EventLoop
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.graphics import *
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.metrics import dp
from kivy.vector import Vector
from kivy.properties import NumericProperty, ObjectProperty
from kivy.logger import Logger

from enemy import Enemy
from player import Player
from powerup import PowerUp
from background import ScrollingBackground


class EnergyApp(App):
    title = 'Energy'

    energy_amount = NumericProperty(0.0)

    enemy_rate = NumericProperty(5)
    powerup_rate = NumericProperty(30)
    score = NumericProperty(0)
    player = ObjectProperty()

    def __init__(self, **kwargs):
        super(EnergyApp, self).__init__(**kwargs)

        self.schedule_next_enemy()
        self.schedule_next_powerup()

    def schedule_next_enemy(self):
        Clock.schedule_once(
            self.create_enemy,
            random.random() * self.enemy_rate
        )

    def create_enemy(self, delta):
        enemy = Enemy()
        pos = self.get_new_spawn_pos()
        enemy.direction = self.get_new_direction(pos)
        enemy.pos = self.get_new_pos(enemy.size, pos)

        self.root.add_widget(enemy)
        self.schedule_next_enemy()

    def schedule_next_powerup(self):
        Clock.schedule_once(
            self.create_powerup,
            random.random() * self.powerup_rate
        )

    def create_powerup(self, delta):
        powerup = PowerUp()
        pos = self.get_new_spawn_pos()
        powerup.direction = self.get_new_direction(pos)
        powerup.pos = self.get_new_pos(powerup.size, pos)

        self.root.add_widget(powerup)
        self.schedule_next_powerup()

    def get_new_spawn_pos(self):
        pos = Vector(self.root.center).rotate(random.randint(0, 360))
        return pos

    def get_new_direction(self, pos):
        direction = (Vector(pos) * -1).normalize() * dp(50)
        return direction

    def get_new_pos(self, size, pos):
        pos = pos + self.root.center - Vector(size) / 2
        return pos

    def build(self):
        Logger.info("Build Window")
        Logger.info("Starting FPS Counter")
        Clock.schedule_once(self.fps_counter, -1)

        Window.size = 720, 1080/2

    def fps_counter(self, delta):
        Logger.info("FPS: " + str(Clock.get_rfps()))
        Clock.schedule_once(self.fps_counter, 1)


if __name__ == '__main__':
    EnergyApp().run()
