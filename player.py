from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import NumericProperty
from kivy.clock import Clock
from kivy.animation import Animation
from kivy.metrics import dp
from kivy.core.window import Window

class Player(FloatLayout):
    min_size = NumericProperty(dp(10/100*Window.width))
    max_size = NumericProperty(dp(23/100*Window.width))
    growth = NumericProperty(0)
    energy = NumericProperty(1.0)
    energy_growth = NumericProperty(0)
    angle = NumericProperty(0)

    def __init__(self, **kwargs):
        super(Player, self).__init__(**kwargs)
        App.get_running_app().player = self
        self.start_animation()
        Clock.schedule_interval(self.update, 0.001)

    def start_animation(self):
        anim = Animation(angle=360, duration=20)
        anim += Animation(angle=360, duration=20)
        anim.repeat = True
        anim.start(self)

    def on_angle(self, item, angle):
        if angle == 360:
            item.angle = 0

    def on_touch_down(self, event):
        self.energy_growth = -0.05
        self.growth = dp(8)

    def on_touch_up(self, event):
        self.energy_growth = 0.03
        self.growth = dp(-8)

    def update(self, delta):

        App.get_running_app().energy_amount = self.energy
        self.width += self.growth * delta
        self.energy += self.energy_growth * delta

        if self.energy <= 0 and self.growth > 0:
            self.energy = 0
            self.growth = 0
            self.energy_growth = 0

        if self.width <= self.min_size:
            self.width = self.min_size
            self.growth = 0
            self.energy_growth = 0.06

        if self.energy >= 1:
            self.energy = 1
            self.energy_growth = 0

        if self.width >= self.max_size:
            self.width = self.max_size

        #print("")
        #print(int(self.energy * 100 ),"%")
        #print(self.energy_growth)
        #print(int(self.width))
        #print(self.growth)
