from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.clock import Clock
from kivy.vector import Vector
from kivy.animation import Animation
from kivy.properties import ListProperty, NumericProperty

import random

class PowerUp(FloatLayout):
    energy_amount = NumericProperty(random.randint(5,20))
    velocity = ListProperty([0, 0])
    angle = NumericProperty(0)

    def __init__(self, **kwargs):
        super(PowerUp, self).__init__(**kwargs)
        self.start_animation()
        Clock.schedule_interval(self.update, 0.05)

    def start_animation(self):
        anim = Animation(angle=360, duration=2)
        anim += Animation(angle=360, duration=2)
        anim.repeat = True
        anim.start(self)

    def on_angle(self, item, angle):
        if angle == 360:
            item.angle = 0

    def update(self, delta):
        app = App.get_running_app()
        pos = Vector(self.pos)
        self.pos = pos + Vector(self.direction) * delta

        if app.player:
            if (
                (Vector(self.center) - app.root.center).length()
                <
                (self.width + app.player.width) /2
            ):
                if app.player.growth < 0:
                    Clock.unschedule(self.update)
                    self.parent.remove_widget(self)
                    app.player.energy += self.energy_amount/100
                else:
                    Clock.unschedule(self.update)
                    self.parent.remove_widget(self)
