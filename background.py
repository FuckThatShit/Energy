from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty, NumericProperty
from kivy.graphics import *
from kivy.core.image import Image as CoreImage
from kivy.clock import Clock

class ScrollingBackground(Widget):
    text_rectangle = ObjectProperty()
    speed = NumericProperty(0.5)
    x_offset = NumericProperty(0)

    def __init__(self, **kwargs):
        super(ScrollingBackground, self).__init__(**kwargs)
        texture = CoreImage("Textures/Background.png").texture
        texture.wrap = 'repeat'

        with self.canvas:
            self.text_rectangle = Rectangle(
                texture=texture,
                size=self.size,
                pos=self.pos
            )

        Clock.schedule_interval(self.update, 0.03)

    def on_size(self, object, size):
        self.text_rectangle.size = size
        x_scale = self.size[0] / float(self.text_rectangle.texture.size[0])
        y_scale = self.size[1] / float(self.text_rectangle.texture.size[1])
        self.text_rectangle.tex_coords = [
            0, y_scale,
            x_scale, y_scale,
            x_scale, 0,
            0, 0
        ]

    def update(self, delta):
        self.text_rectangle.size = self.size
        t = Clock.get_boottime()
        y_incr =  self.speed * -1 * t
        x_scale = self.size[0] / float(self.text_rectangle.texture.size[0])
        y_scale = self.size[1] / float(self.text_rectangle.texture.size[1])

        self.text_rectangle.tex_coords = [
            self.x_offset, y_incr + y_scale,
            self.x_offset + x_scale, y_incr + y_scale,
            self.x_offset + x_scale, y_incr,
            self.x_offset, y_incr
        ]
