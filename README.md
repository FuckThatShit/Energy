# Energy

Welcome Player, in this game you are little spaceship travelling out in unknown galaxies, isolated from any help.
So your are the one, who has to get this ship through all the enemies and junk that is flying out there by managing a shield.
But watch out, the galaxies are big, the enemys evil and the shield is limited!

Textures by emr.o; DerGrumpf

Sounds by maxy.abc

License: GNU GPLv3

Have Questions? Join our Discord!
https://discord.gg/Ba8YqKa

Copyright © 2018 by SpiceInc.
All rights Reserved
