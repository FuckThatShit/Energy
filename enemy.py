from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.clock import Clock
from kivy.vector import Vector
from kivy.properties import ListProperty, NumericProperty

class Enemy(FloatLayout):
    score = NumericProperty(0)
    velocity = ListProperty([0, 0])

    def __init__(self, **kwargs):
       super(Enemy, self).__init__(**kwargs)
       Clock.schedule_interval(self.update, 0.05)

    def update(self, delta):
        app = App.get_running_app()
        pos = Vector(self.pos)
        self.pos = pos + Vector(self.direction) * delta
        self.score = min(5, int(pos.distance(app.root.center) / 50) + 1)

        if app.player:
            if (
                (Vector(self.center) - app.root.center).length()
                <
                (self.width + app.player.width) / 2
            ):
                if app.player.growth > 0:
                    Clock.unschedule(self.update)
                    self.parent.remove_widget(self)
                    app.score += self.score
                else:
                    app.root.remove_widget(app.player)
                    app.player = ""
                    print("Game Over")
